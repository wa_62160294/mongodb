const mongoose = require('mongoose')
mongoose.connect('mongodb://localhost:27017/example')
const Room = require('./models/Room')
const Building = require('./models/Building')

async function main () {
  // Update
  // const room = await Room.findById('621b80d7d992428a0cb61bae')
  // room.capacity = 20
  // room.save()
  // console.log(room)
  const room = await Room.findOne({ capacity: { $gt: 100 } }).populate('building')
  console.log(room)
  console.log('-----------------------')
  const rooms = await Room.find({ capacity: { $lt: 100 } }).populate('building')
  console.log(rooms)
  const building = await Building.find({})
  console.log(JSON.stringify(building))
}

main().then(() => {
  console.log('Finish')
})
